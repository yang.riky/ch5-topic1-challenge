const Movie = require("../models/movie.js");
const fs = require("fs");

class MovieController {

    movies = new Map();

    constructor() {
        this.movies = new Map(Object.entries(JSON.parse(fs.readFileSync("./movies.json", "utf-8"))));
        // async
    }

    #getNewId() {
        let newId = 1;

        if (this.movies.size > 0) {
            newId = Math.max(...this.movies.keys()) + 1;
        }

        return newId;
    }

    addMovie(movie) {
        movie.setId(this.#getNewId());
        this.movies.set(String(movie.getId()), movie);
        this.#writeToJson();
    }

    getMovies(id) {
        if (id) {
            return this.movies.get(id);
        } else {
            return this.movies;
        }
    }

    updateMovie(id, movie) {
        this.movies.set(id, movie);
        this.#writeToJson();
    }

    deleteMovie(id) {
        this.movies.delete(id);
        this.#writeToJson();
    }

    #writeToJson() {
        fs.writeFileSync("./movies.json", JSON.stringify(
            Object.fromEntries(this.movies)
        ));
    }
}

module.exports = MovieController;