class Movie {

    id = 0;
    title = "";
    genres = [];
    duration = 0;
    studio = "";

    constructor(movie) {
        this.title = movie.title;
        this.genres = movie.genres;
        this.duration = movie.duration;
        this.studio = movie.studio;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getTitle() {
        return this.title;
    }

    setTitle(title) {
        this.title = title;
    }

    getGenres() {
        return this.genres;
    }

    setGenres(genres) {
        this.genres = genres;
    }

    getDuration() {
        return this.duration
    }

    setDuration(duration) {
        this.duration = duration;
    }

    getStudio() {
        return this.studio;
    }

    setStudio(studio) {
        this.studio = studio;
    }
}

module.exports = Movie;