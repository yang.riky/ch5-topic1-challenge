const Movie = require("./models/movie.js");
const MovieController = require("./controllers/movieController.js");
const movieController = new MovieController();

function showInstructions() {
    console.log("Instructions: ");
    console.log("1. Add (npm start add title genres1,genres2 duration studio)");
    console.log("2. Show (npm start show or npm start show id)");
    console.log("1. Edit (npm start edit id title genres1,genres2 duration studio)");
    console.log("4. Delete (npm start delete id)");
}

function addMovie(argv) {
    if (argv.length >= 7) {
        const movie = new Movie({
            title: argv[3],
            genres: argv[4].split(','),
            duration: argv[5],
            studio: argv[6]
        });

        movieController.addMovie(movie);

        showMovies();
    } else {
        console.log("Incomplete argument.");
    }
}

function showMovies(argv) {
    let argvLength = 3;

    if (argv) {
        argvLength = argv.length;
    }

    switch (argvLength) {
        case 3:
            console.log(movieController.getMovies());
            break;

        case 4:
            let movie = movieController.getMovies(argv[3]);

            if (movie) {
                console.log(movie);
            } else {
                console.log("Id not found.");
            }
            break;

        default:
            console.log("Incomplete argument.");
            break;
    }
}

function editMovie(argv) {
    if (argv.length >= 8) {
        let id = argv[3];

        if (movieController.getMovies(id)) {
            const movie = new Movie({
                title: argv[4],
                genres: argv[5].split(','),
                duration: argv[6],
                studio: argv[7]
            });

            movie.setId(id);

            movieController.updateMovie(id, movie);

            showMovies();
        } else {
            console.log("Id not found.");
        }
    } else {
        console.log("Incomplete argument.");
    }
}

function deleteMovie(argv) {
    if (argv.length >= 4) {
        movieController.deleteMovie(argv[3]);
        showMovies();
    } else {
        console.log("Incomplete argument.");
    }
}

if (process.argv.length > 2) {
    let instruction = process.argv[2];

    switch (instruction) {
        case "add":
            addMovie(process.argv);
            break;

        case "show":
            showMovies(process.argv);
            break;

        case "edit":
            editMovie(process.argv);
            break;

        case "delete":
            deleteMovie(process.argv);
            break;

        default:
            console.log("Instruction '" + instruction + "' not found.");
            break;
    }
} else {
    showInstructions();
}